#include <iostream>
#include <math.h>
using namespace std;

int isPrime(long num) // Zwraca 1 je�li jest to liczba pierwsza, a zwraca 0 je�li nie.
{
	if (num < 2)
		return 0;

	if (num > 2 && (num % 2) == 0)
		return 0;

	for(int i = 2; i < num; i++ )
	{
		if ( (num % i) == 0)
			return 	0;
	}
	return 1;
}
int main (int argc, char* argv[])
{
	int threshold = 10001, i = 0;
	long prime = 0;
	do 
	{
		if (isPrime(++ prime)) i ++;
	}
	while (i != threshold);
	
	cout << "10 001st Prime is:  " << prime << endl;	
	system("PAUSE");
	return 0;
}
